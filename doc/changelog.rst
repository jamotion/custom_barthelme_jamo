Changelog
=========
Version 8.0.1.13.0
------------------
BAHE-2371:
    - Refactor logic how client order reference is transferred to pickings and invoices

Version 8.0.1.12.1
------------------
Bugfix:
    - Correct name change on all templates, when category is changed to 'EoL'

Version 8.0.1.12.0
------------------
Feature:
    - Added new product category type 'End of Lifecycle'. If chosen, state of
      all products of the category is changed to 'End of Lifecycle' and an
      asterisk is added to product name

Version 8.0.1.11.2
------------------
Bugfix:
    - Improved/fixed processing and displaying of order_confirmation_state

Version 8.0.1.11.1
------------------
Bugfix:
    - Fixed error in create method of product template model

Version 8.0.1.11.0
------------------
Feature 1811:
    - Show field 'client_order_ref' on sale order, quotation and stock picking
      tree view
    - Added field 'order_confirmation_state' on sale order form view to show
      if order confirmation has been sent with or without date

Follow-ups:
    - Show order confirmation state on stock picking tree and form view
    - Made field 'date' in stock picking tree view invisible
    - Set value of 'min_date' on stock picking view equal to 'requested_date'
      of sale order, if the latter is altered manually

Version 8.0.1.10.0
------------------
Feature 1818:
    - Added chatter message for confirmation of delivery on stock.picking

Version 8.0.1.9.0
-----------------
Feature 1807:
    - Added line color depending on product state to product template and
      product product tree views
    - Added filter domain to field 'product_id' on SO-line
    - Show product description ('name') on stock picking form move lines view
    - Add menu item for product.product view to sale, purchase, stock and
      production menu
    - Make menu item to product.product view always visible for all users

Version 8.0.1.8.0
-----------------
Feature 1796:
    - Removed field commitment_date from sale order tree view
    - Field requested_date on sale order tree view shows date only
    - Field min_date on stock picking tree view shows date only

Version 8.0.1.7.0
-----------------
Feature 1788:
    - Add category tags to sale order tree view
    - Replace category tags in sale order form view

Version 8.0.1.6.0
-----------------
Feature 1760:
    - Field 'min_date' made visible on stock.picking tree view

Version 8.0.1.5.0
-----------------
Feature 1644:
    - Ability to filter delivery notes by tags of sale order

Follow-ups:
    - Added search functionality for category tags on sale order search view
    - Added columns for carrier and tags on stock picking tree view
    - Optimizations on stock picking form view:
        - moved fields 'carrier_id', 'priority' and 'sale_categ_ids'
        - underneath 'partner_id'
    - Changed tags are updated reciprocally on sale order view and stock
      picking view

Version 8.0.1.4.0
-----------------
Feature 1501:
    - Additional field 'carrier_advice' on delivery note (model stock.picking)

Version 8.0.1.3.0
-----------------
Feature 1618:
    - Removed state modifiers on following fields to make them always editable:
        - in purchase.order: partner_ref
        - in sale.order: requested_date

Version 8.0.1.2.1
-----------------
Bugfix 1593:
    - Overrule the base field 'min_date' to make it always editable

Version 8.0.1.2.0
-----------------
Feature 1580:
    - Custom german translation of date fields on sale order

Feature 1589:
    - Changed translation of field requested_date to "Versanddatum"

Version 8.0.1.1.0
-----------------
Feature 1535:
    - Custom german translation of field "carrier_id" on delivery price list

Version 8.0.1.0.0
-----------------
Initial Release