# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2015 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by boris on 19.03.18.
#
# imports of python lib
import logging

# imports of openerp
import math

from openerp import models, fields, api, _

# imports from odoo modules

# global variable definitions
_logger = logging.getLogger(__name__)


class AccountInvoice(models.Model):
    # Private attributes
    _inherit = 'account.invoice'

    # Default methods

    # Fields declaration

    # compute and search fields, in the same order that fields declaration

    # Constraints and onchanges

    # CRUD methods
    @api.multi
    def write(self, vals):
        result = super(AccountInvoice, self).write(vals)

        for record in self.filtered(
                lambda f: f.type in ['out_invoice', 'out_refund'] and f.state == 'draft'):
            record.update_vrg_position()

        return result

    # Business methods
    def update_vrg_position(self):
        """
        Iterate over all invoice lines/products of the given invoice. For every product:
         - lookup if a VRG characteristic (factor) is configured for it.
         - calculate the VRG amount for the product by multiplying the configured factor with the VRG product price.
        Afterwards, sum up all VRG amounts of the individual invoice lines.

        Finally,
         - delete any existing vrg invoice line (this solves position/sequence numbering issues)
         - add a new VRG position/invoice line if necessary
        """
        _logger.debug("Update VRG position on invoice.")

        vrg_product = self.env.ref('custom_barthelme_jamo.product_product_vrg')

        for record in self:
            vrg_sum = 0
            for line in record.invoice_line.filtered(
                    lambda f: f.type.code == 'normal' and
                              f.price_subtotal > 0.1):
                characteristic = line.product_id.get_vrg_characteristic()
                if characteristic:
                    vrg_sum += characteristic.value_float * \
                               vrg_product.list_price * math.ceil(
                            line.quantity)

            # lookup VRG invoice line
            vrg_lines = record.invoice_line.filtered(
                    lambda f: f.product_id == vrg_product)
            vrg_line = vrg_lines and vrg_lines[0] or False
            if vrg_sum > 0:
                if vrg_line:
                    vrg_line.price_unit = vrg_sum
                    vrg_line.sequence = 999
                # we need a VRG line - so create it
                else:
                    record._create_new_vrg_invoice_line(vrg_product, vrg_sum)
            elif vrg_lines:
                vrg_lines.write({'state': 'cancel'})
                vrg_lines.unlink()

    def _create_new_vrg_invoice_line(self, vrg_product, vrg_sum):
        self.ensure_one()
        _logger.debug("Create a new VRG position invoice line.")

        product_data = self.env['account.invoice.line'].product_id_change(
            vrg_product.id,
            vrg_product.uom_id.id,
            partner_id=self.partner_id.id,
            fposition_id=self.fiscal_position.id,
            qty=1,
        )

        values = dict()
        values['invoice_id'] = self.id
        values['account_id'] = product_data['value']['account_id']
        values['product_id'] = vrg_product.id
        values['name'] = product_data['value']['name']
        values['invoice_line_tax_id'] = [(6, 0, product_data['value']['invoice_line_tax_id'])]
        values['price_type'] = 'no_trade'  # deactivate trade calculation
        values['quantity'] = 1
        values['price_unit'] = vrg_sum
        values['state'] = 'draft'
        values['sequence'] = 999

        self.env['account.invoice.line'].create(values)
