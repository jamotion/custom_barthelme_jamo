# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2015 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by boris on 09.04.18.
#
# imports of python lib
import logging

# imports of openerp
from openerp import models, fields, api, _
from openerp.osv import fields as old_fields

# imports from odoo modules

# global variable definitions
_logger = logging.getLogger(__name__)


class NoteNote(models.Model):
    # Private attributes
    _inherit = 'note.note'
    
    # Default methods
    
    # Fields declaration
    jamo_stage_id = fields.Many2one(
            comodel_name="note.stage",
            string="Stage",
            ondelete="restrict",
    )
    
    stage_id = fields.Many2one(
            compute='_compute_stage_id',
            inverse='_inverse_stage_id',
            default=lambda self: self.env['note.stage'].search([], limit=1)[0].id,
    )
    
    # compute and search fields, in the same order that fields declaration
    
    def _compute_stage_id(self):
        for record in self:
            record.stage_id = record.jamo_stage_id
    
    def _inverse_stage_id(self):
        for record in self:
            record.jamo_stage_id = record.stage_id
    
    # Constraints and onchanges
    
    # CRUD methods
    @api.model
    @api.returns('self', lambda value: value.id)
    def create(self, vals):
        default_stage_id = self.env.context.get('default_stage_id', False)
        if not vals.get('stage_id', False) and default_stage_id:
            vals['jamo_stage_id'] = default_stage_id
        result = super(NoteNote, self).create(vals)
        new_stage_id = vals.get('stage_id', False)
        if new_stage_id:
            result.message_follower_ids = self.env['note.stage'].browse(
                    new_stage_id).message_follower_ids
        return result

    @api.multi
    def write(self, vals):
        """
        Handle the case, where a note is moved to a new stage.

        Attach all followers from the new state to the current note/s. All
        followers which followed the note
        before moving it to the new stage are removed.
        """
        res = super(NoteNote, self).write(vals)

        new_stage_id = vals.get('stage_id', False)
        if new_stage_id:
            for record in self:
                record.message_follower_ids = self.env['note.stage'].browse(
                        new_stage_id).message_follower_ids
        
        return res
    
    # Action methods
    def read_group(self, cr, uid, domain, fields, groupby, offset=0,
                   limit=None, context=None, orderby=False, lazy=True):
        if groupby and groupby[0] == "stage_id":
            # search all stages
            current_stage_ids = self.pool.get('note.stage').search(
                    cr, uid, [], context=context)
            
            if current_stage_ids:
                stages = self.pool['note.stage'].browse(
                        cr, uid, current_stage_ids, context=context)
                
                result = [{
                    '__context': {'group_by': groupby[1:]},
                    '__domain': domain + [('jamo_stage_id.id', '=', stage.id)],
                    'stage_id': (stage.id, stage.name),
                    'jamo_stage_id': (stage.id, stage.name),
                    'stage_id_count': self.search(cr, uid, domain + [
                        ('jamo_stage_id', '=', stage.id)], context=context,
                                                  count=True),
                    '__fold': stage.fold,
                } for stage in stages]

            else:  # if stage_ids is empty
                result = [{  # notes for unknown stage
                    '__context': {'group_by': groupby[1:]},
                    '__domain': domain,
                    'stage_id': False,
                    'jamo_stage_id': False,
                    'stage_id_count': 0
                }]
            return result
        
        else:
            return super(NoteNote, self).read_group(
                    cr, uid, domain, fields,
                    groupby,
                    offset=offset,
                    limit=limit,
                    context=context,
                    orderby=orderby,
                    lazy=lazy)

# Business methods
