# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2004-2017 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by sebi.pulver on 21.07.2017.
#
from openerp import models, fields, api
import logging

_logger = logging.getLogger(__name__)


class MailComposeMessage(models.Model):
    _inherit = 'mail.compose.message'

    @api.multi
    def send_mail(self):
        self.ensure_one()
        sale_order = self.env['sale.order'].browse(self.env.context.get(
            'default_res_id'))
        if self.env.context.get('default_model') == 'sale.order' \
                and self.env.context.get('default_res_id') \
                and self.env.context.get('mark_so_as_sent') \
                and sale_order.state == 'progress':
            if sale_order.requested_date:
                sale_order.order_confirmation_state = \
                    'requested_date_confirmed'
            else:
                sale_order.order_confirmation_state = \
                    'requested_date_unknown'
        return super(MailComposeMessage, self).send_mail()
