# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2015-2017 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by renzo.meister on 13.06.2015.
#
from openerp import models, fields, api, _
from openerp.osv import osv, fields as oldfields
import logging

_logger = logging.getLogger(__name__)


class StockPicking(osv.osv):
    # Private attributes
    _inherit = 'stock.picking'

    # Default methods

    # Fields declaration
    carrier_advice = fields.Char(
        string='Carrier Advice',
    )

    client_order_ref = fields.Char(
        string='Reference/Description',
        related='sale_id.client_order_ref',
    )

    order_confirmation_state = fields.Selection(
        selection=[
            ('requested_date_unknown', 'Sent without Date'),
            ('requested_date_confirmed', 'Sent with Date'),
        ],
        related='sale_id.order_confirmation_state',
    )

    reference_name = fields.Char(
        string='Reference Name',
    )

    sale_id = fields.Many2one(
        comodel_name='sale.order',
        string='Sale id',
        compute='_compute_sale_id',
        store=True,
    )

    sale_categ_ids = fields.Many2many(
        related='sale_id.categ_ids',
        readonly=False,
        store=True,
    )

    section_id = fields.Many2one(
        related='sale_id.section_id',
        readonly=True,
    )

    date_done_date = fields.Date(
            string='Delivered',
            compute='_compute_date_done_date',
            store=True
    )

    # compute and search fields, in the same order as fields declaration
    @api.multi
    @api.depends('date_done')
    def _compute_date_done_date(self):
        for record in self:
            record.date_done_date = fields.Date.to_string(fields.Date.from_string(record.date_done))

    @api.multi
    @api.depends('origin')
    def _compute_sale_id(self):
        for record in self.filtered(lambda f: f.origin):
            record.sale_id = self.env['sale.order'].search([
                ('name', '=', record.origin)], limit=1)

    # Constraints and onchanges

    # CRUD methods
    @api.model
    def _get_invoice_vals(self, key, inv_type, journal_id, move):
        result = super(StockPicking, self)._get_invoice_vals(key, inv_type, journal_id, move)
        if move.picking_id.client_order_ref:
            result['name'] = move.picking_id.client_order_ref
        else:
            result['name'] = move.picking_id.reference_name
        return result

    def get_min_max_date(self, cr, uid, ids, field_name, arg, context=None):
        return super(StockPicking, self).get_min_max_date(
            cr, uid, ids, field_name, arg, context)

    def _set_min_date(self, cr, uid, id, field, value, arg, context=None):
        return super(StockPicking, self)._set_min_date(
            cr, uid, id, field, value, arg, context)

    @api.cr_uid_ids_context
    def _get_pickings_dates_priority(self, cr, uid, ids, context=None):
        res = set()
        for move in self.browse(cr, uid, ids, context=context):
            if move.picking_id and \
                (not (move.picking_id.min_date < move.date_expected
                                               < move.picking_id.max_date)
                 or move.priority > move.picking_id.priority):
                res.add(move.picking_id.id)
        return list(res)

    # Action methods

    # Business methods
    @api.cr_uid_ids_context
    def do_transfer(self, cr, uid, picking_ids, context=None):
        result = super(StockPicking, self).do_transfer(
            cr, uid, picking_ids, context)
        pickings = self.browse(cr, uid, picking_ids, context)
        for picking in pickings:
            picking.message_post(body=_(u'The Picking has been delivered'))

        return result

    # Fields declaration old api
    _columns = {
        'min_date': oldfields.function(
            get_min_max_date,
            multi='min_max_date',
            fnct_inv=_set_min_date,
            store={
                'stock.move': (
                    _get_pickings_dates_priority,
                    ['date_expected', 'picking_id'],
                    20)
            },
            type='datetime',
            string='Scheduled Date',
            select=1,
            states={},
            help='Scheduled time for the first part of the shipment to be '
                 'processed. Setting manually a value here would set it '
                 'as expected date for all the stock moves.',
            track_visibility='onchange'),
    }


class StockMove(models.Model):
    # Private attributes
    _inherit = 'stock.move'

    # Default methods

    # Fields declaration

    # compute and search fields, in the same order as fields declaration

    # Constraints and onchanges

    # CRUD methods

    # Action methods

    # Business methods
    @api.multi
    def action_confirm(self):
        result = super(StockMove, self).action_confirm()
        self[0].picking_id.write({
            'reference_name':
                self[0].procurement_id.sale_line_id.order_id.client_order_ref
        })
        return result
