# -*- coding: utf-8 -*-
from . import stock
from . import purchase
from . import sale_order_dates
from . import sale_order_line
from . import product
from . import mail_compose_message
from . import sale_order
from . import res_partner
from . import account_invoice
from . import crm_phonecall
from . import note_stage
from . import note_note
