# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2017 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by sebi.pulver on 26.06.2017.
#
# imports of python lib
import logging

# imports of openerp
from openerp import models, fields, api, _

# imports from odoo modules

# global variable definitions
VRG_PRODUCT_NAME = 'VRG'

_logger = logging.getLogger(__name__)


class ProductTemplate(models.Model):
    # Private attributes
    _inherit = 'product.template'

    # Default methods

    # Fields declaration

    # compute and search fields, in the same order as fields declaration

    # Constraints and onchanges

    # CRUD methods
    @api.model
    @api.returns('self', lambda value: value.id)
    def create(self, vals):
        if 'state' in vals:
            for record in self:
                for variant in record.product_variant_ids:
                    variant.variant_state = vals['state']
        return super(ProductTemplate, self).create(vals)

    @api.multi
    def write(self, vals):
        if 'state' in vals:
            for record in self:
                for variant in record.product_variant_ids:
                    variant.variant_state = vals['state']
        return super(ProductTemplate, self).write(vals)

    # Action methods

    # Business methods


class ProductProduct(models.Model):
    # Private attributes
    _inherit = 'product.product'

    # Default methods

    # Fields declaration
    variant_state = fields.Selection(
        selection=[
            ('', ''),
            ('draft', 'In Development'),
            ('sellable', 'Normal'),
            ('end', 'End of Lifecycle'),
            ('obsolete', 'Obsolete'),
        ],
        string='Status',
    )

    # compute and search fields, in the same order as fields declaration

    # Constraints and onchanges

    # CRUD methods

    # Action methods

    # Business methods
    @api.multi
    def get_vrg_characteristic(self):
        """
        Returns the VRG characteristics of the given product if one is defined ore 'None' otherwise.
        """
        self.ensure_one()
        for c in self.effective_product_characteristics:
            if c.characteristic_id.name == VRG_PRODUCT_NAME:
                return c
        return None

    @api.model
    def add_minimum_stock_rules_to_products(self):
        """
        Finds all products of type 'product' which can be purchased and have not yet any stock rules defined.
        Creates minimum stock rule for matching products.
        :return:
        """
        _logger.info('JAMO: Cron: Add minimum stock rules to all products')

        products_to_create_rules_for = self.search([
            ('purchase_ok', '=', True),
            ('type', 'in', ['product']),
            ('orderpoint_ids', '=?', False)
        ])

        products_to_create_rules_for.create_reordering_rule()

    @api.multi
    def create_reordering_rule(self):
        _logger.info('JAMO: Creating minimum stock rules for {} products'.format(len(self)))

        warehouse_id = self.env['sale.order']._get_default_warehouse()

        for record in self:
            values = dict()
            values['product_id'] = record.id
            values['product_min_qty'] = 0
            values['product_max_qty'] = 0
            values['qty_multiple'] = 1
            values['warehouse_id'] = warehouse_id
            values['product_uom'] = record.uom_id

            record.env['stock.warehouse.orderpoint'].create(values)
            

class ProductCategory(models.Model):
    # Private attributes
    _inherit = 'product.category'

    # Default methods

    # Fields declaration
    type = fields.Selection(
        selection_add=[
            ('end', 'End of Lifecycle'),
        ],
    )

    # compute and search fields, in the same order as fields declaration

    # Constraints and onchanges

    # CRUD methods
    @api.multi
    def write(self, vals):
        product_templates = self.env['product.template'].search([(
            'categ_id', '=', self.id)])
        if 'type' in vals and vals['type'] == 'end':
            for template in product_templates:
                template.name += '*'
                template.state = 'end'
        return super(ProductCategory, self).write(vals)

    # Action methods

    # Business methods