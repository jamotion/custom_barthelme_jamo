# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2004-today Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by sebi.pulver on 21.07.2017.
#

from openerp import models, fields, api
import openerp.addons.decimal_precision as dp
from openerp.tools.float_utils import float_round
import math
import logging

_logger = logging.getLogger(__name__)


class SaleOrder(models.Model):
    # Private attributes
    _inherit = 'sale.order'
    
    # Default methods
    
    # Fields declaration
    order_confirmation_state = fields.Selection(
        selection=[
            ('requested_date_unknown', 'Sent without Date'),
            ('requested_date_confirmed', 'Sent with Date'),
        ],
        string='Order Confirmation State',
    )

    important_note = fields.Html(
        related='partner_id.important_note',
        readonly=True,
    )

    external_note = fields.Html(
        related='partner_id.external_note',
        readonly=True,
    )

    sales_forecast = fields.Float(
        string='Sales forecast',
        compute='_compute_sales_forecast',
        store=True,
        digits_compute=dp.get_precision('Account'),
    )

    expecting_order_percentage = fields.Integer(
        string='Expecting order %',
    )

    num_phonecalls = fields.Integer(
        compute='_compute_num_phonecalls',
    )
    
    # Compute and search methods, in the same order as fields declaration
    @api.multi
    @api.depends('amount_untaxed', 'expecting_order_percentage')
    def _compute_sales_forecast(self):
        prec = self.env['decimal.precision'].precision_get('Account')
        for record in self:
            record.sales_forecast = float_round(
                    record.amount_untaxed * record.expecting_order_percentage /
                    100,
                    precision_rounding=prec)
    
    @api.multi
    def _compute_num_phonecalls(self):
        for record in self:
            record.num_phonecalls = self.env['crm.phonecall'].search_count(
                    [('sale_order_id', '=', record.id)])
    
    # Constraints and onchanges

    # CRUD methods
    @api.multi
    def write(self, vals):
        result = super(SaleOrder, self).write(vals)
        for record in self:
            if record.state in ['draft', 'sent']:
                record.update_vrg_position()

            if not record.picking_ids and not record.invoice_ids:
                return result
            
            record.picking_ids.filtered(lambda f: f.state not in ['cancel', 'done']).write({
                'min_date': record.requested_date,
            })

            if 'client_order_ref' in vals:
                record.picking_ids.write({
                    'reference_name': record.client_order_ref,
                })

                record.invoice_ids.filtered(lambda f: f.state not in ['open', 'paid', 'cancel']).write({
                    'name': record.client_order_ref,
                })
        
        return result
    
    # Action methods
    @api.multi
    def button_dummy(self):
        """
        Triggers the recalculation of the VRG position when the
        'Aktualisieren' button is clicked.
        This is also necessary here - as the code in the 'write' is only
        triggered, when really changing something
        on the order-line (e.g. quantity) and not on the product (e.g.
        adding a vrg characteristic)
        """
        self.update_vrg_position()
        return True
    
    @api.multi
    def action_create_phonecall(self):
        """
        Open phonecall view to schedule an new phonecall
        :return dict: dictionary value for created meeting view
        """
        res = self.env['ir.actions.act_window'].for_xml_id(
                'custom_barthelme_jamo', 'action_create_phonecall')
        res['context'] = {
            'default_partner_phone': self.partner_id.phone,
            'default_sale_order_id': self.id,
            'default_partner_id': self.partner_id.id
        }
        return res

    # Business methods
    def delivery_set(self, cr, uid, ids, context=None):
        if not context:
            context = {}
        context['vrg_no_update'] = True
        return super(SaleOrder, self).delivery_set(cr, uid, ids,
                                                   context=context)
    
    @api.multi
    def update_vrg_position(self):
        """
        Iterate over all sale order lines/products of the given order. For
        every product:
         - lookup if a VRG characteristic (factor) is configured for it.
         - calculate the VRG amount for the product by multiplying the
         configured factor with the VRG product price.
        Afterwards, sum up all VRG amounts of the individual order lines.

        Finally,
         - delete any existing vrg order line (this solves position/sequence
         numbering issues)
         - add a new VRG position/order line if necessary
        """
        _logger.debug("Update VRG position on sale order.")
        vrg_product = self.env.ref('custom_barthelme_jamo.product_product_vrg')
        
        for record in self:
            vrg_sum = 0
            
            for line in record.order_line.filtered(
                    lambda f: f.type.code == 'normal' and
                              f.price_subtotal > 0.1):
                characteristic = line.product_id.get_vrg_characteristic()
                if characteristic:
                    vrg_sum += characteristic.value_float * \
                               vrg_product.list_price * math.ceil(
                            line.product_uom_qty)
            
            # lookup VRG order line and delete it. Recreation assures,
            # that the vrg line is always the last position
            # in the order lines. This can happen, when a new product is
            # added, when a VRG line already exist.
            vrg_lines = record.order_line.filtered(
                    lambda f: f.product_id == vrg_product)
            vrg_line = vrg_lines and vrg_lines[0] or False
            if vrg_sum > 0:
                if vrg_line:
                    vrg_line.price_unit = vrg_sum
                    vrg_line.sequence = 999
                else:
                    # we need a VRG line - so create it
                    record._create_new_vrg_order_line(vrg_product, vrg_sum)
            elif vrg_lines:
                vrg_lines.write({'state': 'cancel'})
                vrg_lines.unlink()
    
    @api.multi
    def _create_new_vrg_order_line(self, vrg_product, vrg_sum):
        self.ensure_one()
        _logger.debug(
                "Create a new VRG sale order line with price: {}.".format(
                        vrg_sum))
        
        product_data = self.env['sale.order.line'].product_id_change_with_wh(
                self.pricelist_id.id,
                vrg_product.id,
                partner_id=self.partner_id.id,
                qty=1,
                price_type='no_trade',
                fiscal_position=self.fiscal_position.id,
        )
        
        values = dict()
        values['order_id'] = self.id
        values['product_id'] = vrg_product.id
        values['tax_id'] = [(6, 0, product_data['value']['tax_id'])]
        values['price_type'] = 'no_trade'  # deactivate trade calculation
        values['product_uom_qty'] = 1
        values['price_unit'] = vrg_sum
        values['state'] = 'done'
        values['sequence'] = 999
        values['name'] = vrg_product.name
        
        self.env['sale.order.line'].create(values)
